#include <avr/interrupt.h>

#define output_low(port,pin) port &= ~(1<<pin)
#define output_high(port,pin) port |= (1<<pin)
#define set_input(portdir,pin) portdir &= ~(1<<pin)
#define set_output(portdir,pin) portdir |= (1<<pin)
#define SETBIT(ADDRESS,BIT) (ADDRESS |= (1<<BIT))
#define CLEARBIT(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT))
#define CHECKBIT(ADDRESS,BIT) (ADDRESS & (1<<BIT))


#define up !CHECKBIT( PINF, 5 )
#define left !CHECKBIT( PINF, 6 )
#define right !CHECKBIT( PINF, 0 )
#define down !CHECKBIT( PINF, 4 )
#define ok !CHECKBIT( PINF, 1 )

volatile uint16_t time_spent;


void led_b_on();
void led_b_off();

void led_c_on();
void led_c_off();

void led_d_on();
void led_d_off();

//LCD meetodid
void start_screen();
void settings();
bool direction();
uint8_t timeselection();
uint8_t degree();
void running( uint16_t, bool, uint16_t );
void edit_timing_cursor_addr(bool *, uint8_t *, uint8_t * );
void edit_time( bool , uint8_t *, uint8_t *);
void write_mins( uint8_t );
void write_secs( uint8_t );
void flip_indicator(uint8_t , uint8_t );
void write_deg( uint8_t );
uint8_t mins_and_secs_to_seconds( uint8_t , uint8_t );
uint16_t degree_to_steps( uint8_t );
void init_timer();

//optocouper
void shoot();

// nupud
void init_button();