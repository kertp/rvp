#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>
#include "rvp.h"

#define DIRECTION 3
#define DELAY 1500

uint8_t turn_steps(uint16_t , bool );
void do_step();
void set_up_stepper();
void set_direction( bool );
void set_clockwise();
void set_ccw();