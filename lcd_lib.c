//*****************************************************************************
//
// File Name	: 'lcd_lib.c'
// Title		: 4 bit LCd interface
// Author		: Scienceprog.com - Copyright (C) 2007
// Created		: 2007-06-18
// Revised		: 2007-06-18
// Version		: 1.0
// Target MCU	: Atmel AVR series
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
//*****************************************************************************
#include "lcd_lib.h"
#include <inttypes.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

void LCDsendChar(uint8_t ch)		//Sends Char to LCD
{
	LCDbusy();
	LDP=( ( ch  & 0b11110000 ) >> 1 );
	SETBIT( LCP, LCD_RS );
	LCDwriteData();
	CLEARBIT( LCP, LCD_RS );

	LCDbusy();
	LDP=( ( ch & 0b00001111 ) << 3 );
	SETBIT( LCP, LCD_RS );
	LCDwriteData();
	CLEARBIT( LCP, LCD_RS );
}

void LCDsendCommand(uint8_t cmd)	//Sends Command to LCD
{
	LCDbusy();
	LDP = ( ( cmd & 0b11110000 ) >> 1);
	LCDwriteData();

	LCDbusy();
	LDP = ( ( cmd & 0b00001111 ) << 3 );	
	LCDwriteData();
}

void LCDinit(void)//Initializes LCD
{
	_delay_ms(150);
	LDP=0x00;
	LCP=0x00;
	LDDR|=1<<LCD_D7|1<<LCD_D6|1<<LCD_D5|1<<LCD_D4;
	LCDR|=1<<LCD_E|1<<LCD_RW|1<<LCD_RS;

   //---------one------
	LDP=0<<LCD_D7|0<<LCD_D6|1<<LCD_D5|1<<LCD_D4; //4 bit mode
	LCP|=1<<LCD_E|0<<LCD_RW|0<<LCD_RS;		
	LCDwriteData();

	//-----------two-----------
	LDP=0<<LCD_D7|0<<LCD_D6|1<<LCD_D5|1<<LCD_D4; //4 bit mode
	LCP|=1<<LCD_E|0<<LCD_RW|0<<LCD_RS;		
	LCDwriteData();

	//-------three-------------
	LDP=0<<LCD_D7|0<<LCD_D6|1<<LCD_D5|0<<LCD_D4; //4 bit mode
	LCP|=1<<LCD_E|0<<LCD_RW|0<<LCD_RS;		
	LCDwriteData();

	LCDsendCommand(0b00101100);
	LCDsendCommand(0b00001000);
	LCDsendCommand(0b00000001);
	LCDsendCommand(0b00001100);
}

void LCDclr(void)				//Clears LCD
{
	LCDsendCommand( 1 << LCD_CLR );
}

void LCDhome(void)			//LCD cursor home
{
	LCDsendCommand( 1 << LCD_HOME );
}
void LCDstring(uint8_t* data, uint8_t nBytes)	//Outputs string to LCD
{
register uint8_t i;

	// check to make sure we have a good pointer
	if (!data) return;

	// print data
	for(i=0; i<nBytes; i++)
	{
		LCDsendChar(data[i]);
	}
}

void LCDcursorOn(void) //displays LCD cursor
{
	LCDsendCommand(0x0E);
}
void LCDcursorOnBlink(void)	//displays LCD blinking cursor
{
	LCDsendCommand(0x0F);
}
void LCDcursorOff(void)	//turns OFF cursor
{
	LCDsendCommand(0x0C);
}

void LCDbusy(){
	// PB4 - PB6 to input
	LDDR &= 0x87;
	CLEARBIT( LCP, LCD_RS );

	SETBIT( LCP, LCD_RW );
	SETBIT( LCP, LCD_E );
	_delay_us( 100 );
	// Kui PINB on kõrge, siis see tähendab, et Busy flag on kõrge ja seega on displei hõivatd
	// Süüan ledi PB7
	while( CHECKBIT( PINB, LCD_BUSY ) ){
		CLEARBIT( LCP, LCD_E );
		SETBIT( LCP, LCD_E );
		_delay_us( 100 );
	}
	CLEARBIT( LCP, 7 );
	CLEARBIT( LCP, LCD_E );

	// PORT B to output once again
	LDDR = 0XFF;
}

void LCDwriteData(){
	LCP |= 1 << LCD_E;
	_delay_ms( 10 );
	LCP&=~( 1 << LCD_E );
	_delay_ms( 10 );
}

void LCDsetAddr( uint8_t addr ){
	addr |= (1 << 7);
	LCDsendCommand(addr);
}
