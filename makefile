
GCC=avr-gcc
OBJCOPY=avr-objcopy
DFUPROG=dfu-programmer
CPU=atmega32u4
FREQ=2000000UL
MAIN=main
OBJDUMP=avr-objdump

create:
	$(GCC) -w -O1 -DF_CPU=$(FREQ) -mmcu=$(CPU) -c -o $(MAIN).o $(MAIN).c -include rvp.c -include lcd_lib.c -include stepper.c -std=c99 -DDEBUG
	$(GCC) -w -mmcu=$(CPU) $(MAIN).o -o $(MAIN)
	$(OBJDUMP) -s -D -M intel -m avr:6  main.o > main.asm 
	$(OBJCOPY) -O ihex $(MAIN) $(MAIN).hex -j .text -j .data

program:
	$(DFUPROG) $(CPU) flash $(MAIN).hex
	$(DFUPROG) $(CPU) start

clean:
	$(DFUPROG) $(CPU) erase

erase:
	rm $(MAIN).o $(MAIN).hex $(MAIN) $(MAIN).asm -f

all:
	make clean
	make create
	make program


