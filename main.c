// /*
//  * testrvp.c
//  *
//  * Created: 14.12.2013 20:24:10
//  *  Author: Kristian
//  */ 
#define F_CPU 2000000UL  // 16 MHz
#include <util/delay.h>
#include <avr/io.h>
#include <stdbool.h>
#include <avr/interrupt.h>
#include "rvp.h"
#include "stepper.h"
#include "lcd_lib.h"

ISR(TIMER1_COMPA_vect) 
{
  time_spent += 1;
  if(time_spent > 5){
    led_d_on();
  }
  else{
    led_d_off();
  }
}

int main()
{
  init_timer();
  set_up_stepper();
  LCDinit();
  init_button();
  LCDclr();
  LCDhome();
  start_screen();
  while( 1 );
  return 0;
}
