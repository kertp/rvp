#include <inttypes.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <stdbool.h>
#include <string.h>
#include "rvp.h"
#include "lcd_lib.h"
#include "stepper.h"

void led_b_on(){
  set_output( DDRB, 7 );
  SETBIT( PORTB, 7 );
}

void led_b_off(){
  CLEARBIT( PORTB, 7 );
}

void led_c_on(){
  set_output( DDRC, 6 );
  SETBIT( PORTC, 6 );
}

void led_c_off(){
  CLEARBIT( PORTC, 6 );
}

void led_d_on(){
  set_output( DDRD, 5 );
  SETBIT( PORTD, 5 ); 
}

void led_d_off(){
  CLEARBIT( PORTD, 5 ); 
}

void start_screen(){
  LCDclr();
  LCDcursorOff();
  LCDsetAddr(0x05);
  LCDstring("START", 5);
  LCDsetAddr(0x44);
  LCDstring("SETTINGS", 8);
  
  bool choice = 0;
  flip_indicator(0x03, 0x43);
  while( 1 ){
    if( up || down ){
      choice = !choice;
      if( choice ){
        flip_indicator(0x43, 0x03);
      }
      else{
        flip_indicator(0x03, 0x43);
      }
      while( up || down);
    }
    if( ok )
      break;
  }
  if( choice )
    settings();
  else
    running( 800, true, 10 );
}

void settings(){
  
  uint8_t seconds = timeselection();
  bool clockwise = direction();
  uint8_t degrees = degree();
  uint16_t steps = degree_to_steps( degrees );
  running( steps, clockwise, seconds );
}

uint8_t timeselection(){
  LCDclr();
  LCDsetAddr(0x06);
  LCDstring("TIME", 4);

  LCDsetAddr( 0x44 );
  LCDstring(" m   sec", 8);

  uint8_t mins = 0;
  uint8_t secs = 1;

  LCDsetAddr( 0x44 );
  char time_buffer[2];
  itoa( mins, time_buffer, 2 );
  LCDsendChar(time_buffer[0]);
  write_mins( mins );
  write_secs( secs );

  LCDsetAddr( 0x44 );
  LCDcursorOnBlink();

  bool min_location = 1;
  while( 1 ){
    edit_timing_cursor_addr( &min_location, &mins, &secs );
    edit_time( min_location, &mins, &secs );
    if( ok )
      break;
  }
  return mins_and_secs_to_seconds( mins, secs );
}

bool direction(){
  LCDclr();
  LCDcursorOff();
  LCDsetAddr(0x00);
  LCDstring("DIRECTION", 9);
  LCDsetAddr(0x0C);
  LCDstring("CCW", 3);
  LCDsetAddr(0x4D);
  LCDstring("CW", 2);

  flip_indicator(0x0B, 0x4B);
  bool ccw = 1;
  while( 1 ){
    if( up || down ){
      if( ccw ){
        ccw = 0;
        flip_indicator( 0x4B, 0x0B );
      }
      else{
        ccw = 1;
        flip_indicator(0x0B, 0x4B);
      }
      while( up || down );
    }
    if( ok )
      break;
  }
  return !ccw;
}

uint8_t degree(){
  LCDclr();
  LCDsetAddr(0x00);
  LCDstring("DEGREES", 7);
  LCDsetAddr(0x48);
  LCDstring("deg", 3);
  uint8_t deg = 0;
  write_deg( deg );
  
  LCDsetAddr(0x44);
  LCDcursorOnBlink();
  while( 1 ){
    if( up ){
      if( deg < 180 )
        deg += 1;
      else
        deg = 0;
      write_deg( deg );
    }
    else if( down ){
      if( deg > 0)
        deg -= 1;
      else
        deg = 179;
      write_deg( deg ); 
    }
    if( ok )
      break;
  }
  return deg;
}

void running( uint16_t steps, bool clockw, uint16_t seconds ){
  TCNT1 = 0;
  time_spent = 0;
  LCDclr();
  LCDsetAddr(0x00);
  LCDstring("TAKING PHOTOS", 13);
  LCDsetAddr(0x40);
  LCDstring("PRESS OK TO STOP", 16);
  while( 1 ){

    if( !turn_steps( steps, clockw ) )
      break;
    while( seconds > time_spent ){
      if( ok )
        break;
    }
    shoot();
  }
  start_screen();
}

void shoot(){
  set_output( DDRE, 6 );
  CLEARBIT( PORTE, 6 );
  _delay_ms(100);
  SETBIT( PORTE, 6 );
  TCNT1 = 0;
  time_spent = 0;
}

void init_button(){
  DDRF = 0x00;
  PORTF = 0xFF;
  uint8_t vajutus = 0b10000000;
  MCUCR = vajutus;
  MCUCR = vajutus;
}

void edit_timing_cursor_addr(bool *min_location, uint8_t *mins, uint8_t *secs ){
  if( left || right ){ 
    if( *min_location ){
      *min_location = 0;
      write_secs( *secs );
    }
    else{
      *min_location = 1;
      write_mins( *mins );
    }
    while( left || right );
  }
}

void edit_time( bool min_location, uint8_t *mins, uint8_t *secs ){
  if( up ){
    if( !min_location ){
      if( *secs >= 59)
        *secs = 1;
      else
        *secs += 1;
      write_secs( *secs );  
    }
    else{
      if( *mins >= 9)
        *mins = 0;
      else
        *mins += 1;
      write_mins( *mins );  
    }
  }
  else if(down){
    if( !min_location ){
      if( *secs <= 0)
        *secs = 59;
      else
        *secs -= 1;
      write_secs( *secs );  
    }
    else{
      if( *mins <= 0)
        *mins = 9;
      else
        *mins -= 1;
      write_mins( *mins );  
    }
  }
}

void write_mins( uint8_t mins ){
  LCDcursorOff();
  LCDsetAddr( 0x44 );
  char str[2];
  // radix - arvusüsteem
  itoa( mins, str, 10);
  LCDsendChar( str[0] );
  LCDsetAddr( 0x44 );
  LCDcursorOnBlink();
}

void write_secs( uint8_t secs ){
  LCDcursorOff();
  LCDsetAddr( 0x47 );
  char str[3];
  // radix - arvusüsteem
  itoa( secs, str, 10);
  // saadame eraldi, sest muidu end char lõpus
  if(secs < 10){
    str[1] = str[0];
    str[0] = '0';
  }
  LCDsendChar( str[0] );
  LCDsendChar( str[1] );
  LCDsetAddr( 0x47 );
  LCDcursorOnBlink();
}

void flip_indicator(uint8_t first_addr, uint8_t second_addr){
  LCDsetAddr(second_addr);
  LCDsendChar(' ');
  LCDsetAddr(first_addr); 
  LCDsendChar('>');
}

void write_deg( uint8_t deg ){
  LCDcursorOff();
  LCDsetAddr( 0x44 );
  char str[4];
  itoa( deg, str, 10);
  // push
  if( deg < 10){
    str[2] = str[0];
    str[1] = '0';
    str[0] = '0';
  }
  else if( deg < 100){
    str[2] = str[1];
    str[1] = str[0];
    str[0] = '0';
  }
  LCDsendChar( str[0] );
  LCDsendChar( str[1] );
  LCDsendChar( str[2] );
  LCDsetAddr( 0x44 );
  LCDcursorOnBlink();
}

uint8_t mins_and_secs_to_seconds( uint8_t mins, uint8_t seconds){
  return 60 * mins + seconds;  
}

uint16_t degree_to_steps( uint8_t degrees ){
  return 8 * degrees;
}

void init_timer(){
  // CTC mode
  TCCR1B |= (1 << WGM12);
  // 2 MHz / 64
  TCCR1B |= (1 << CS11) | ( 1 << CS10);
  // luba katkestused
  TIMSK1 |= (1 << OCIE1A);
  OCR1A   = 31250;
  time_spent = 0;
  sei();
}