#include "stepper.h"

uint16_t total_steps = 0;

uint8_t turn_steps(uint16_t steps, bool clockwise) {
  set_direction( clockwise );
  while(steps != 0 ) {
    if( ok )
      return 0;
    do_step();
    steps -= 1;
    total_steps += 1;
    // tagasipööramine
    if( total_steps >= 3200 ){
      set_direction( !clockwise );
      while( total_steps > 0 ){
        // KUi OKi vajutatakse, siis lahkun funktsioonist
        if( ok )
          return 0;
        do_step();
        total_steps -= 1;
      }
      set_direction( clockwise );
      TCNT1 = 0;
      time_spent = 0;
    }
  }
  return 1;
}

void do_step( ){
  SETBIT( PORTD, 2 );
  _delay_us( DELAY );
  CLEARBIT( PORTD, 2 );
  _delay_us( DELAY );
}

void set_up_stepper(){
  DDRD = 0xFF;
  PORTD |= ( 1 << 0 );
  PORTD |= ( 1 << 1 );
  PORTD |= ( 1 << 4 );
  PORTD |= ( 1 << 7 );
}

void set_direction( bool clockwise ){
  if(clockwise)
    set_clockwise();
  else
    set_ccw();
}

void set_clockwise(){
  CLEARBIT( PORTD, DIRECTION );
}

void set_ccw(){
  SETBIT( PORTD, DIRECTION );
}