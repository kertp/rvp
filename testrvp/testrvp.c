/*
 * testrvp.c
 *
 * Created: 14.12.2013 20:24:10
 *  Author: Kristian
 */ 
#define F_CPU 16000000UL  // 16 MHz
#include <util/delay.h>
#include <avr/io.h>
#include "lcd_lib.h"

int main()
{
// 	DDRB  = 0b10000000;
// 	PORTB = 0b10000000;
	DDRC  = 0b11111111;
	PORTC = 0b01000000;
	DDRD  = 0b11111111;
	PORTD = 0b00100000;
	_delay_ms(100);
	DDRB  = 0b11111111;
	LCDinit();
	_delay_ms(100);
	while(1) {
			LCDcursorOnBlink();
			_delay_ms(100);
			LCDcursorOFF();
			_delay_ms(100);
	}
	return 0;
}


